import { IonGrid, IonIcon, IonRow, IonItem, IonLabel, IonInput, IonDatetime, IonButton } from '@ionic/react';
import React from 'react';
import { person, mail, calendar, globe, pin, lock } from 'ionicons/icons';
import './Account.scss';
import api from '../../../api/index';
import { AuthContext } from '../../../components/auth/index';




class Account extends React.Component {
    static contextType = AuthContext;

    constructor() {
        super();
        // this.formEl = React.createRef();
        this.state = {
            errors: ['']
        };
    }

    render() {
        const { currentUser } = this.context;
        const form_info = [
            {text: 'Name', icon: person, placeholder: currentUser.displayName},
            {text: 'Email', icon: mail, placeholder: currentUser.email},
            {text: 'Birthday', icon: calendar, placeholder: currentUser.birthday},
            {text: 'Bio', icon: globe, placeholder: 'Edit Bio'},
            {text: 'Location', icon: pin, placeholder: 'Iasi, momentan'},
            {text: 'Password', icon: lock, placeholder: ''},
        ];
        
        const create_grid_rows = form_info.map((info, i) => {
                return <IonRow className = 'grid-rows'  key={i}>
                            <IonItem className = 'grid-items' color = 'light'>
                                    <IonIcon icon = {info.icon}></IonIcon>
                                    <IonLabel className = 'grid-label'>{info.text}</IonLabel>
                                    {info.text !== 'Birthday' ? 
                                    <IonInput 
                                        className = 'grid-input' 
                                       
                                        autofocus = {i === 0 ? 'true' : 'false'}
                                        value = {info.placeholder}
                                        clearOnEdit = 'true'
                                        name = {info.text}
                                        type = {info.text === 'Password' ? 'password' : info.text === 'Email' ? 'email' : 'text'}
                                        required={true}
                                        autocomplete = 'false' 
                                    ></IonInput>
                                    : <IonDatetime
                                        value="2020-05-14T15:43"
                                        name="date"
                                        displayFormat="MMM DD, YYYY HH:mm"
                                    />
                                }
                                    
                            </IonItem>
                        </IonRow>
                });
        const handle_save = (e => {
            e.preventDefault();
            const { currentUser } = this.context;
            let formData = new FormData(e.target);
            let name = formData.get('Name');
            let email = formData.get('Email');
            let birthday = formData.get('date');
            let bio = formData.get('Bio');
            let location = formData.get('Location');
            let password = formData.get('Password');
            //console.log(name, email, birthday, bio, location);
            api.updateUserProfile(currentUser.uid, name, email, birthday, bio, location,password);
        })
        return(
            <>
                <form onSubmit = {handle_save} className = 'grid-form' method = 'POST'>
                <IonGrid
                    fixed = 'false'
                    className = 'the_grid'
                >
                    <IonRow>
                        <IonItem className = 'grid-items grid-title' color = 'light'>
                            <IonLabel>Profile Editor</IonLabel>
                        </IonItem>
                    </IonRow>
                    {create_grid_rows}

                    <IonButton
						expand="full"
						type="submit"
                        style={{position: 'absolute', bottom: 0, left: 0, margin: 0, width: '50%',height:'8vh', borderRight: '1px solid white'}}
                        >
						Submit Changes
					</IonButton>

					<IonButton
						expand="full"
						className="button secondary"
						style={{position: 'absolute', bottom: 0, right: 0, margin: 0, width: '50%',height:'8vh' , borderLeft: '1px solid white'}}
                         onClick={e => {
                            e.preventDefault();
                            const { history } = this.props;
							history.push('/home/account');
						}}
					>
						Cancel Changes
					</IonButton>
                </IonGrid>
                </form>
            </>
        )
    }
}

export default Account;