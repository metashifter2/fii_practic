import React from 'react';
import { withRouter } from 'react-router';
import { handleEnterKeypress } from '../../../utils/handlers';
import { ROUTE_LOGIN } from '../../../utils/routes';

import api from '../../../api';

import RegistrationForm from '../../../components/registrationForm';
import logo from '../../../assets/img/restaurant-circular.svg';

class Registration extends React.Component {
    constructor() {
        super();
        this.formEl = React.createRef();
        this.state = {
            errors: ['']
        };
    }

    componentDidMount() {
        document.addEventListener('keydown', this.handleEnterKeypresWrapper);
    }

    componentWillUnmount() {
        document.removeEventListener('keydown', this.handleEnterKeypresWrapper);
    }

    handleEnterKeypresWrapper = ev => {
        handleEnterKeypress(ev, () => {
            this.formEl.current.dispatchEvent(new Event('submit'));
        });
    };

    handleSignUp = async event => {
        event.preventDefault();
        const { firstName, lastName, email, password } = event.target.elements;
        try {
            const { history } = this.props;
            //console.log(firstName, lastName, email, password);
            await api.onSignUpUser(
                firstName.value,
                lastName.value,
                email.value,
                password.value
            );
            history.push(ROUTE_LOGIN);
        } catch (error) {
            console.log(error);
            const { errors } = this.state;
            this.setState({
                errors: [...errors, error.message]
            });
        }
    };

    onChange = () => {
        const { errors } = this.state;
        this.setState({
            errors: [...errors, '']
        });
    };

    render() {
        const { errors } = this.state;
        return (
            <>
                <header className="header">
                    <div
                        className="header__logo"
                        style={{ backgroundImage: `url(${logo})` }}
                    />
                    <div>
                        <h3>
                            eat<span>together</span>
                        </h3>
                    </div>
                </header>
                <RegistrationForm
                    formRef={this.formEl}
                    errorMessage={errors[errors.length - 1]}
                    onSubmit={this.handleSignUp}
                    onChange={this.onChange}
                />
            </>
        );
    }
}

export default withRouter(Registration);