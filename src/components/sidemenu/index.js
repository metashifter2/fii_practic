import React from 'react';
import {
    IonAvatar,
    IonButton,
    IonContent,
    IonIcon,
    IonItem,
    IonLabel,
    IonList,
    IonMenu,
    IonMenuToggle
} from '@ionic/react';
import { map, person, sunny } from 'ionicons/icons';
import { withRouter } from 'react-router';

import { ROUTE_ACCOUNT, ROUTE_MAP } from '../../utils/routes';

import './Sidemenu.scss';
import api from '../../api';

import { AuthContext } from '../auth';

const pages = [
    { title: 'Account', path: ROUTE_ACCOUNT, icon: person },
    { title: 'Map', path: ROUTE_MAP, icon: map }
];


class Sidemenu extends React.Component {
    static contextType = AuthContext;

    componentDidMount() {
          
    }

    renderMenuItems = () => {
        const { history, location } = this.props;
        return pages.map(page => (
            <IonMenuToggle key={page.title}>
                <div
                    className={
                        page.path === location.pathname
                            ? 'sidemenu__item--activated'
                            : ''
                    }
                >
                    <IonItem button onClick={() => history.push(page.path)}>
                        <IonIcon slot="start" icon={page.icon} />
                        <IonLabel>{page.title}</IonLabel>
                    </IonItem>
                </div>
            </IonMenuToggle>
        ));
    };

    render() {
        const rootElement = document.getElementsByTagName('body');
        const { id } = this.props;
        const { currentUser } = this.context;

        // let testReturnImgUrl = api.fetchUserImage(currentUser.uid);
        // console.log(testReturnImgUrl);

        return (
            <IonMenu
                swipeGesture={false}
                side="start"
                content-id={id}
                className="sidemenu"
            >
                <IonContent>
                    <div className="sidemenu__header">
                        <IonAvatar onClick = { ()=> { const { history } = this.props;history.push(ROUTE_ACCOUNT) }}>
                            <img
                                src={ api.fetchUserImage(currentUser.uid) }
                                alt="avatar"
                            />
                        </IonAvatar>
                        <IonLabel className="sidemenu__name">
                            {currentUser.displayName}
                        </IonLabel>
                    </div>
                    <hr className="sidemenu__separator" />
                    <IonList>{this.renderMenuItems()}</IonList>
                    <ion-list className = 'sidemenu__mode'>
                        <ion-item lines="full">
                        <IonIcon icon = {sunny}></IonIcon>
                        <ion-label>
                            Toggle Dark Theme
                        </ion-label>
                        <ion-toggle id="themeToggle" slot="end" onClick = {() => {rootElement[0].classList.toggle('dark')}} checked = 'false'></ion-toggle>
                        </ion-item>
                    </ion-list>
                    <IonButton
                        className="sidemenu__signout"
                        onClick={api.logout}
                    >
                        Sign out
                    </IonButton>
                </IonContent>
            </IonMenu>
        );
    }
}

export default withRouter(Sidemenu);
