import firebaseProvider from '../config/Firebase';

const auth = firebaseProvider.auth();
const db = firebaseProvider.firestore();

const api = {
    onSignUpUser: (
        signUpFirstName,
        signUpLastName,
        signUpEmail,
        signUpPassword
    ) => {
        return auth
            .createUserWithEmailAndPassword(signUpEmail, signUpPassword)
            .then(
                rsp => {
                    const { user } = rsp;
                    api.createNewAccount({
                        userId: user.uid,
                        signUpFirstName,
                        signUpLastName,
                        signUpEmail
                    }).then(() => {
                        auth.currentUser.updateProfile({
                            displayName: `${signUpFirstName} ${signUpLastName}`
                        });
                        return {
                            message:
                                'Your account it was successfully registered'
                        };
                    });
                },
                err => {
                    throw err;
                }
            );
    },
    createNewAccount: ({ userId, ...rest }) => {
        const body = {
            firstName: rest.signUpFirstName,
            lastName: rest.signUpLastName,
            email: rest.signUpEmail,
            img: 'https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y',
            //i know it is useless, but...
            id: userId
        };
        return  db.doc(`users/${userId}`).set(body);
    },
    onSignInUser: (email, password) => {
        return auth.signInWithEmailAndPassword(email, password).then(
            rsp => {
                console.log(rsp);
                return rsp;
            },
            err => {
                throw err;
            }
        );
    },
    logout: () => {
        const auth = firebaseProvider.auth();
        return auth.signOut();
    },
    fetchRestaurant: id => {
        return db
            .collection('/restaurants')
            .where('api_id', '==', id)
            .get();
    },
    fetchEventsForRestaurant: id => {
        return db
            .collection('/events')
            .where('restaurant', '==', id)
            .get();
    },
    fetchParticipantsCountForEvent: id => {
        return db
            .collection('/participants')
            .where('event', '==', id)
            .get();
    },
    joinEvent: (eventId, userId) => {
        return db.collection('/participants').add({
            event: eventId,
            user: userId
        });
    },
    leaveEvent: (eventId, userId) => {
        return db
            .collection('/participants')
            .where('event', '==', eventId)
            .where('user', '==', userId)
            .get()
            .then(querySnapshot => {
                querySnapshot.forEach(doc => {
                    doc.ref
                        .delete()
                        .then(() => {
                            console.log('Document successfully deleted!');
                        })
                        .catch(function(error) {
                            console.error('Error removing document: ', error);
                        });
                });
            })
            .catch(function(error) {
                console.log('Error getting documents: ', error);
            });
    },
    createEventForRestaurant: (event) => {
        return db.collection('/events').add(event);
    },
    fetchUserImage: (userId) => {
        
        return db
        .collection('/users')
        .where('id','==',userId)
        .get()
        .then(querySnapshot => {
            querySnapshot.forEach(doc => {
                        console.log(doc.data().img);
                        return doc.data().img
            });
        })
        .catch(function(error) {
            console.log('Error getting documents: ', error);
        });
    },
    updateUserProfile: (userId, name, email, birthday, bio, location, password) => {
        auth.currentUser.updateProfile({
            displayName: name,
            birthday:birthday,
            bio:bio,
            location:location,
        });
        const body = {
            name:name,
            email:email,
            birthday:birthday,
            bio:bio,
            location:location,
        }
        db.collection('/users').doc(userId).get()
        .then(querySnapshot => {
            querySnapshot.forEach(doc => {
                        return doc.set(body)
            });
        })
        .catch(function(error) {
            console.log('Error getting documents: ', error);
        });

        auth.currentUser.updateEmail({
            email:email
        });
        auth.signInWithEmailAndPassword(email, password)
        .then(function(userCredential) {
        userCredential.user.updateEmail(email)
    })
    }
};

export default api;

/**
 * db
            .collection('/users')
            .where('id', '==', userId)
            .get()
            .then(function(querySnapshot) {
                querySnapshot.forEach(doc => {
                    console.log(doc.id, " => ", doc.data().img);
                    return doc.data().img;
                    //imageURL = doc.data().img
                });
            })
            .catch(function(error) {
                console.log("Error getting documents: ", error);
            })
 */

 /**
  * //let imageURL = undefined;
        let docRef = db.collection('users').doc(userId);
        return docRef.get().then(doc => {
            if (doc.exists) {
                console.log("Document data:", doc.data());
                //imageURL = doc.data().img;
                return doc.data().img;
            } else {
                console.log("No such document!");
            }
        }).catch(function(error) {
            console.log("Error getting document:", error);
        });
  */