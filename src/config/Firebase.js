import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/messaging';

const config = {
    apiKey: "AIzaSyC9LPdH7CkbRMCi9Kz7rtTlsN3-vklijqM",
    authDomain: "eat-togheter-fii-practic.firebaseapp.com",
    databaseURL: "https://eat-togheter-fii-practic.firebaseio.com",
    projectId: "eat-togheter-fii-practic",
    storageBucket: "eat-togheter-fii-practic.appspot.com",
    messagingSenderId: "871925346720",
    appId: "1:871925346720:web:802f4581e0a32816e034c5",
    measurementId: "G-8Y21K77Y3N"
};

const firebaseProvider = firebase.initializeApp(config);
export default firebaseProvider;
